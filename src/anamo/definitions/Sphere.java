/**
 * Copyright 2014, Vijay Lakshminarayanan
 *
 * All rights reserved.
 */
package anamo.definitions;

/**
 * @author Vijay Lakshminarayanan <laksvij@gmail.com>
 */
public class Sphere extends SimpleObject {

    public Sphere(double radius) {
        super(1, 1, 1, -1 * (radius*radius));
    }

    /**
     * Normal vector of a point on the surface of a sphere is the
     * vector from the origin of the sphere to the point.
     *
     * @see anamo.definitions.SimpleObject#getNormalVector(anamo.definitions.Point3D)
     */
    @Override
    protected Point3D getNormalVector(Point3D pt) {
        double dx = p0.x - pt.x;
        double dy = p0.y - pt.y;
        double dz = p0.z - pt.z;

        return new Point3D(dx, dy, dz);
    }
}
