/**
 * Copyright 2014, Vijay Lakshminarayanan
 *
 * All rights reserved.
 */
package anamo.definitions;

/**
 * @author Vijay Lakshminarayanan <laksvij@gmail.com>
 */
public class Point3D {
    public static final Point3D ORIGIN = new Point3D(0, 0, 0);
    public static final Point3D INFINITY = new Point3D(
            Double.POSITIVE_INFINITY,
            Double.POSITIVE_INFINITY,
            Double.POSITIVE_INFINITY);

    public final double x;
    public final double y;
    public final double z;

    /**
     * @param x
     * @param y
     * @param z
     */
    public Point3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double dot(Point3D p) {
        Point3D p1 = this;
        Point3D p2 = p;

        return (p1.x * p2.x) + (p1.y * p2.y) + (p1.z * p2.z);
    }

    public Point3D plus(Point3D p) {
        return new Point3D(x + p.x, y + p.y, z + p.z);
    }

    public Point3D minus(Point3D p) {
        return this.plus(p.times(-1));
    }

    public Point3D times(double v) {
        return new Point3D(x * v, y * v, z * v);
    }

    public Point3D withX(double v) {
        return new Point3D(v, y, z);
    }

    public Point3D withY(double v) {
        return new Point3D(x, v, z);
    }

    public Point3D withZ(double v) {
        return new Point3D(x, y, v);
    }

    public boolean isAtInfinity() {
        if (Double.isInfinite(x)
                || Double.isNaN(x)
                || Double.isInfinite(y)
                || Double.isNaN(y)
                || Double.isInfinite(z)
                || Double.isNaN(z))
        {
            return true;
        }

        return false;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return String.format("(%.2f, %.2f, %.2f)", x, y, z);
    }
}
