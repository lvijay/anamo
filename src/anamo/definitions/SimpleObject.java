/**
 * Copyright 2014, Vijay Lakshminarayanan
 *
 * All rights reserved.
 */
package anamo.definitions;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * An object of the form a(x-x0)^2+b(y-y0)^2+c(z-z0)^2+d=0
 *
 * @author Vijay Lakshminarayanan <laksvij@gmail.com>
 */
public abstract class SimpleObject {
    public Point3D p0;
    public final double a;
    public final double b;
    public final double c;
    public final double d;

    /**
     * Create a SimpleObject with offset at the origin.
     *
     * @param a
     * @param b
     * @param c
     * @param d
     */
    public SimpleObject(double a, double b, double c, double d) {
        this(Point3D.ORIGIN, a, b, c, d);
    }

    /**
     * @param p0
     * @param a
     * @param b
     * @param c
     * @param d
     */
    public SimpleObject(Point3D p0,
            double a, double b, double c, double d)
    {
        this.p0 = p0;
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    public void setOffset(Point3D p0) {
        this.p0 = p0;
    }

    /**
     * @param p1
     * @param p2
     * @return list of intersections
     */
    public List<Point3D> intersections(Point3D p1, Point3D p2) {
        double A = a * square(p2.x - p1.x)
                + b * square(p2.y - p1.y)
                + c * square(p2.z - p1.z);
        double B = 2 * a * (p1.x - p0.x) * (p2.x - p1.x)
                + 2 * b * (p1.y - p0.y) * (p2.y - p1.y)
                + 2 * c * (p1.z - p0.z) * (p2.z - p1.z);
        double C = a * square(p1.x - p0.x)
                + b * square(p1.y - p0.y)
                + c * square(p1.z - p0.z)
                + d;

        // u = -b +/- sqrt(b^2-4ac)
        //          2a

        double det = square(B) - 4 * A * C;

        if (det < 0) {
            return Collections.emptyList();
        }

        double upd = (-B + Math.sqrt(det)) / (2 * A); // +ve determinant
        double und = (-B - Math.sqrt(det)) / (2 * A); // -ve determinant

        double ixp = p1.x + upd * (p2.x - p1.x);
        double ixn = p1.x + und * (p2.x - p1.x);
        double iyp = p1.y + upd * (p2.y - p1.y);
        double iyn = p1.y + und * (p2.y - p1.y);
        double izp = p1.z + upd * (p2.z - p1.z);
        double izn = p1.z + und * (p2.z - p1.z);

        Point3D i0 = new Point3D(ixp, iyp, izp);
        Point3D i1 = new Point3D(ixn, iyn, izn);

        if (det == 0) {
            return Collections.singletonList(i0);
        }

        return Arrays.asList(i0, i1);
    }

    /**
     * @param P
     *            point representing a pixel on the original image.
     *            Assumed to be within the object. Not verified.
     * @param V
     *            point representing the viewpoint (the eye). Assumed
     *            to be outside the object. Not verified.
     * @return the point on the XY plane which represents the
     *         reflection of the line from V to P. Returns
     *         {@link Point3D#INFINITY} if there's no point of
     *         reflection.
     */
    public Point3D reflectionPoint(Point3D P, Point3D V) {
        Point3D i = getIntersectionPoint(P, V);

        if (i.isAtInfinity()) {
            return Point3D.INFINITY;
        }

        Point3D r = reflectionVector(P, V, i);
        double t = i.z / r.z;

        return new Point3D(i.x - t * r.x, i.y - t * r.y, 0);
    }

    protected Point3D getIntersectionPoint(Point3D P, Point3D V)  {
        List<Point3D> ixns = intersections(P, V);

        if (ixns.isEmpty()) {
            return Point3D.INFINITY;
        }

        return ixns.get(0);
    }

    protected Point3D reflectionVector(Point3D P, Point3D V, Point3D i) {
        Point3D n = getNormalVector(i);
        Point3D v = V.minus(P);
        Point3D a = n.times(n.dot(v) / n.dot(n)).minus(v);
        Point3D r = v.plus(a.times(2));

        return r;
    }

    protected abstract Point3D getNormalVector(Point3D pt);

    public static final double square(double x) {
        return x * x;
    }
}
