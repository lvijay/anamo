/**
 * Copyright 2014, Vijay Lakshminarayanan
 *
 * All rights reserved.
 */
package anamo.definitions;

/**
 * @author Vijay Lakshminarayanan <laksvij@gmail.com>
 */
public class Cylinder extends SimpleObject {

    public Cylinder(double radius) {
        super(1, 1, 0, -1 * (radius*radius));
    }

    /**
     * @see anamo.definitions.SimpleObject#getNormalVector(anamo.definitions.Point3D)
     */
    @Override
    protected Point3D getNormalVector(Point3D pt) {
        return pt.withZ(0.0d);
    }

}
