/**
 * Copyright 2014, Vijay Lakshminarayanan
 *
 * All rights reserved.
 */
package anamo.anamorph;

import static java.lang.Math.round;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.image.BufferedImage;

import anamo.definitions.Point3D;
import anamo.definitions.SimpleObject;

/**
 * @author Vijay Lakshminarayanan <laksvij@gmail.com>
 */
public class Anamorpher {
    private final SimpleObject o;

    public Anamorpher(SimpleObject o) {
        this.o = o;
    }

    public BufferedImage anamorph(BufferedImage img, Point3D eye) {
        Point3D v = eye;

        int lowx = Integer.MAX_VALUE, lowy = Integer.MAX_VALUE;
        int maxx = Integer.MIN_VALUE, maxy = Integer.MIN_VALUE;

        int picWidth = img.getWidth();
        int picLenth = img.getHeight();
        int[][] rgb = toPixelArray(img);
        int wadjuster = picWidth / 2;
        int hadjuster = 0;
        int[][] aposx = new int[picWidth][picLenth];
        int[][] aposy = new int[picWidth][picLenth];
        for (int i = 0; i < picWidth; ++i) {
            for (int j = 0; j < picLenth; ++j) {
                Point3D p = new Point3D(0, i - wadjuster, j + hadjuster);
                Point3D rp = o.reflectionPoint(p, v);

                if (rp.isAtInfinity()) {
                    continue;
                }
                aposx[i][j] = (int) round(rp.x);
                aposy[i][j] = (int) round(rp.y);

                if (aposx[i][j] < lowx) { lowx = aposx[i][j]; }
           else if (aposx[i][j] > maxx) { maxx = aposx[i][j]; }
                if (aposy[i][j] < lowy) { lowy = aposy[i][j]; }
           else if (aposy[i][j] > maxy) { maxy = aposy[i][j]; }
            }
        }

        // now normalize coordinates by transposing
        int xr = maxx - lowx + 1;
        int yr = maxy - lowy + 1;

        int xadj = -lowx;
        int yadj = -lowy;

        // translate result image's coordinates to origin
        for (int i = 0; i < picWidth; ++i) {
            for (int j = 0; j < picLenth; ++j) {
                aposx[i][j] += xadj;
                aposy[i][j] += yadj;
            }
        }

        BufferedImage image = new BufferedImage(xr, yr, BufferedImage.TYPE_INT_RGB);

        // set all pixels to white
        for (int i = 0; i < xr - 1; ++i) {
            for (int j = 0; j < yr - 1; ++j) {
                image.setRGB(i, j, Color.white.getRGB());
            }
        }

        Graphics g = image.getGraphics();

        // smoothen colors across the image
        for (int i = 0; i < picWidth; ++i) {
            for (int j = 0; j < picLenth; ++j) {
                if ((i == picWidth - 1)
                        || (j == picLenth - 1)) {
                    continue;
                }

                shade(aposx, aposy, i, j, rgb, g);
            }
        }

        g.dispose();

        return image;
    }

    /**
     * Returns average color of the 4 given colors. Handling needed to
     * avoid overflow and "bleed" into other colors.
     *
     * @param rgb0
     * @param rgb1
     * @param rgb2
     * @param rgb3
     * @return average color of the given colors.
     */
    public int averageColor(int rgb0, int rgb1, int rgb2, int rgb3) {
        int r0 = rgb0 & 0xFF0000;
        int r1 = rgb1 & 0xFF0000;
        int r2 = rgb2 & 0xFF0000;
        int r3 = rgb3 & 0xFF0000;

        int ar = ((r0 + r1 + r2 + r3) / 4) & 0xFF0000;

        int g0 = rgb0 & 0x00FF00;
        int g1 = rgb1 & 0x00FF00;
        int g2 = rgb2 & 0x00FF00;
        int g3 = rgb3 & 0x00FF00;

        int ag = ((g0 + g1 + g2 + g3) / 4) & 0x00FF00;

        int b0 = rgb0 & 0x0000FF;
        int b1 = rgb1 & 0x0000FF;
        int b2 = rgb2 & 0x0000FF;
        int b3 = rgb3 & 0x0000FF;

        int ab = ((b0 + b1 + b2 + b3) / 4) & 0x0000FF;

        return ar | ag | ab;
    }

    /*
     * Given point (i, j), find 3 adjacent points on the anamorphed
     * image; form a convex quadrilateral and fill it with the average
     * color of the 4 points.
     */
    private void shade(int[][] aposx, int[][] aposy,
            int i, int j,
            int[][] rgb,
            Graphics g)
    {
        int anx0 = aposx[i][j];
        int any0 = aposy[i][j];
        int anx1 = aposx[i+1][j];
        int any1 = aposy[i+1][j];
        int anx2 = aposx[i+1][j+1];
        int any2 = aposy[i+1][j+1];
        int anx3 = aposx[i][j+1];
        int any3 = aposy[i][j+1];

        int c0 = rgb[i][j];
        int c1 = rgb[i+1][j];
        int c2 = rgb[i][j+1];
        int c3 = rgb[i+1][j+1];
        int ac = averageColor(c0, c1, c2, c3);

        Polygon p = new Polygon();

        p.addPoint(anx0, any0);
        p.addPoint(anx1, any1);
        p.addPoint(anx2, any2);
        p.addPoint(anx3, any3);

        g.setColor(new Color(ac));
        g.drawPolygon(p);
        g.fillPolygon(p);
    }

    public static int[][] toPixelArray(BufferedImage img) {
        int width = img.getWidth();
        int height = img.getHeight();
        int[][] pixels = new int[width][height];

        for (int i = 0; i < width; ++i) {
            for (int j = 0; j < height; ++j) {
                pixels[i][j] = img.getRGB(width-i-1, height-j-1);
            }
        }
        return pixels;
    }
}
